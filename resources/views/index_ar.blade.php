<!DOCTYPE html>

<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>أعمال التدعيم</title>
    <meta
        content="أعمال التدعيم للمقاولات والإنشاءات والأبنية في السعودية من أفضل المؤسسات التي تقدم خدمات تدعيم المباني وإصلاح الخرسانة  والتسليح باستخدام القمصان الخرسانية والأوتاد الخرسانية، وأعمال كاربون فايبر، وأيضا تقدم خدمات علاج التربة ( حقن التربة و سند التربة ) لمعرفة المزيد تصفح موقعنا تواصل معنا"
        name="description">
    <meta
        content="أعمال التدعيم، أعمال التدعيم للمقاولات، مؤسسة أعمال التدعيم للمقاولات في السعودية ، حقن التربة ، سند التربة ، علاج التربة ، تدعيم مباني ، تدعيم مبنى ، إصلاح خرسانة ، اصلاح خرسانة ، رش اسمنت ، رش خرسانة ، كاربون فايبر ، تقييم مباني ، مؤسسه اعمال التدعيم للمقاولات ، اعمال حقن التربة ، تدعيم المباني ، سند جوانب الحفر ، معالجه هبوط المباني ، ترميم و تدعيم وصيانه المباني ، تدعيم الاعمده ، تدعيم القواعد ، تدعيم الاساسات ، تدعيم الكمرات ، تدعيم البلاطات ، تدعيم الاسقف ، اعمال القمصان الخرسانيه ، اعمال الكاربون فايبر ،اعمال الحديد الفولاذي ، الخرسانه المقذوفة ، الاوتاد الخرسانيه ، المواد الرابطه الايبوكسي والجروات ، تثقيب الخرسانه ، معالجة صدا الحديد ، معالجة الشروخ الخرسانيه ، دهان الحديد ضدا الصدا"
        name="keywords">

    <meta content="فريق مؤسسة أعمال التدعيم" name="author">
    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link rel="alternate" hreflang="ar" href="https://supports-works.com/ar" />
    <link rel="canonical" href="https://supports-works.com/ar" />

    <!-- Google Fonts -->
    <link rel="preconnect" href="{{ asset('https://fonts.googleapis.com') }}">
    <link rel="preconnect" href="{{ asset('https://fonts.gstatic.com') }}" crossorigin>
    <link
        href="{{ asset('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap') }}"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
</head>

<body>
    <!--  Header  -->
    <header id="header" class="header d-flex align-items-center">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="{{ route('ar') }}" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <img src="{{ asset('assets/img/favicon.png') }}" alt="أعمال التدعيم">
                <h1>أعمال التدعيم<span>.</span></h1>
            </a>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="{{ route('ar') }}" class="nav-link active">الرئيسية</a></li>
                    <li><a href="{{ route('ar') }}#about" class="nav-link">من نحن</a></li>
                    <li><a href="{{ route('ar') }}#services" class="nav-link">خدماتنا</a></li>
                    <li><a href="{{ route('ar') }}#projects" class="nav-link">مشاريعنا</a></li>
                    <li><a href="{{ route('ar') }}#contact" class="nav-link">تواصل معنا</a></li>
                    <li><a href="{{ route('switched-en') }}" class="nav-link">
                            English
                        </a>
                    </li>
                    <li>
                        <a href="tel:0966552319919" class="btn-primary">
                            اتصل بنا
                        </a>
                    </li>
                </ul>

            </nav><!-- .navbar -->

        </div>
    </header>
    <!-- End Header -->

    <!--  Hero Section  -->
    <section id="hero" class="hero">
        <div class="info d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h2 data-aos="fade-down">مرحباً بكم في مؤسسة أعمال التدعيم</h2>
                        <p data-aos="fade-up">مؤسسة أعمال التدعيم للمقاولات في المملكة العربية السعودية للتطور العمراني
                            في جميع مجالاته.</p>
                        <a data-aos="fade-up" data-aos-delay="200"
                            href="{{ route('home') }}/files/support_works_profile.pdf" Download="support_works_profile"
                            class="btn-get-started">الملف التعريفي</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000" dir="ltr">
            <div class="carousel-item active"
                style="background-image: url(assets/img/hero-carousel/hero-carousel-1.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-2.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-3.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-4.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-5.jpg)">
            </div>

            <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
            </a>

            <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </section><!-- End Hero Section -->

    <main id="main">

        <!--  About us Section  -->
        <section id="about" class="alt-services">
            <div class="container" data-aos="fade-up">

                <div class="row justify-content-around gy-4">
                    <div role="img" class="col-lg-6 img-bg"
                        style="background-image: url(assets/img/alt-services.jpg);" data-aos="zoom-in"
                        data-aos-delay="100"
                        aria-label="مؤسسة أعمال التدعيم للمقاولات في المملكة العربية السعودية للتطور العمراني في جميع مجالاته.">
                    </div>

                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                        <h3>من نحن</h3>
                        <p>مؤسسة أعمال التدعيم للمقاولات في المملكة العربية السعودية التي تضم مجموعة من الشباب المحترفين
                            والفنيين المتخصصين في بناء وتطوير المنشآت في كافة مجالاتها التجارية والصناعية والسكنية، نقدم
                            العديد من الخدمات، ونتميز بعدة ميزات تجعلك تختارنا، منها:
                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="100">
                            <i class="bi bi-patch-check flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">جودة في العمل والمعدات</span>
                                </h4>
                                <p>نمتلك معدات حديثة تضمن لك جودة الأداء، وأيضا نتابع أحدث التطورات العمرانية عالمياً،
                                    وأحدث الطرق المتبعة لضمان جودة عالية، ونضع ما لدينا من خبرات فنية ومعدات إصلاح حديثة
                                    بين أيديكم آملين في جعلها أداة إيجابية.</p>
                            </div>
                        </div><!-- End Icon Box -->

                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="200">
                            <i class="bi bi-patch-check flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">خبرة واحترافية</span>
                                </h4>
                                <p>مؤسسة أعمال التدعيم تمتلك فريق خبير في تقييم خطورة مشاكل المنشآت بدقة واحترافية،
                                    وإيجاد حل سريع وفني ومتابعة عملية الإصلاح.</p>
                            </div>
                        </div><!-- End Icon Box -->

                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="300">
                            <i class="bi bi-brightness-high flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">حلول إبداعية</span>
                                </h4>
                                <p>مؤسسة أعمال التدعيم تتميز بإبداعها بإيجاد حلول فعالة وسريعة لترضي عملاؤها وتحافظ على
                                    قواعد وأصول عملها، وهذه الحلول تشمل الأبنية و معالجة التربة وكل ما تقدمه من خدمات
                                    مميزة.</p>
                            </div>
                        </div><!-- End Icon Box -->

                    </div>
                </div>

            </div>
        </section><!-- End Alt Services Section -->

        <!--  Services Section  -->
        <section id="services" class="services section-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>خدماتنا</h2>
                    <p>تقدم مؤسسة أعمال التدعيم العديد من الخدمات الخاصة بالمنشآت المعمارية ومعالجة التربة في أعلى جودة
                        وأقصر وقت ممكن.</p>
                </div>

                <div class="row gy-4">

                    <div id="service1" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="service-item  position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-mountain-city"></i>
                            </div>
                            <h3>إصلاح الخرسانة</h3>
                            <p>نقدم خدمة إصلاح خرسانة متصدعة لأي سبب كان، وإعادة تأهيل الأماكن المتضررة وبشكل جذري، وهذه
                                الخدمة لا يمكن حصرها بطريقة معينة، حيث أن كل عنصر إنشائي وكل مشكلة تختلف بالإصلاح عن
                                غيرها.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service2" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-arrow-up-from-ground-water"></i>
                            </div>
                            <h3>التدعيم بنظام القمصان الخرسانية</h3>
                            <p>تتم عن طريق زيادة المقاطع الخرسانية للأعمدة والجسور الحاملة وذلك بإضافة شبكة من حديد
                                التسليم وتزريعه بالخرسانة القديمة حسب المواصفات المطلوبة ويتم صب الخرسانات وفق الشروط
                                المناسبة.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service3" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-compass-drafting"></i>
                            </div>
                            <h3>أعمال الكاربون فايبر</h3>
                            <p>يتم تهيئة العنصر الإنشائي لتحمل قوى أكبر من قوة تحمله القديمة، أو في حالة ضعفه نتيجة
                                عوامل معينة، من خلال معالجته بدلاً عن تكبير مقطعه الخرساني أو وضع مقاطع حديدية لتقويته،
                                باستخدام مواد من شركات عالمية لتقوية العناصر الإنشائية سواء كانت خرسانة مسلحة أو خرسانة
                                مجهدة أو عناصر حديدية.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service4" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-trowel-bricks"></i>
                            </div>
                            <h3>التدعيم بنظام الأوتاد الخرسانية</h3>
                            <p>نقدم هذه الخدمة في حال هبوط الأساسات والأرضيات، وذلك بعمل أوتاد خرسانية مسلحة لنقل حمولات
                                المبنى من القواعد إلى طبقات التربة الصالحة للتأسيس، تخترق هذه الأوتاد أساسات المبنى حتى
                                الوصول للطبقة الصالحة للتأسيس وتصبح القواعد عندها بمثابة قبعة لهذه الأوتاد.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service5" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-helmet-safety"></i>
                            </div>
                            <h3>حقن التربة</h3>
                            <p>أحد أهم أعمال معالجة التربة هو حقن التربة باستخدام مونة اسمنتية ومواد كيميائية لرفع جهد
                                الخرسانة، وبذلك تحصل على منطقة محقونة وخالية من الفراغات والتكهفات.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service6" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="600">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-arrow-up-from-ground-water"></i>
                            </div>
                            <h3>سند التربة</h3>
                            <p>في حال صعوبة صب الخرسانة الجاهزة، نقدم خدمة سند التربة حيث نقوم برش إسمنت لمختلف الأعمال
                                باستخدام أحدث المعدات، وفي ذلك يتم توفير تكاليف ووقت عمل الشدة الخشبية.</p>
                        </div>
                    </div><!-- End Service Item -->

                </div>

            </div>
        </section><!-- End Services Section -->

        <!--  Our Projects Section  -->
        <section id="projects" class="projects">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>مشاريعنا</h2>
                    <p>تم إنجاز أكثر من مئتي مشروع في مختلف المجالات وفي عدة أماكن في المملكة، نقدم لكم نبذة عما قدمناه
                    </p>
                </div>

                <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry"
                    data-portfolio-sort="original-order">

                    <ul class="portfolio-flters" data-aos="fade-up" data-aos-delay="100">
                        <li data-filter="*" class="filter-active">الكل</li>
                        <li data-filter=".filter-concrete-spraying">رش خرسانة</li>
                        <li data-filter=".filter-concrete-reinforcement">تدعيم خرسانة</li>
                        <li data-filter=".filter-soil-injection">حقن التربة</li>
                        <li data-filter=".filter-soil-stabilization">سند التربة</li>
                    </ul><!-- End Projects Filters -->

                    <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-spraying">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project1.jpg" class="img-fluid"
                                    alt="تم إنجاز هذا المشروع بدقة متناهية وإتقان، حيث قمنا  برش الخرسانة للخزانات الاحتياطية.">
                                <div class="portfolio-info">
                                    <h4>رش خرسانة لشركة</h4>
                                    <p>تم إنجاز هذا المشروع بدقة متناهية وإتقان، حيث قمنا برش الخرسانة للخزانات
                                        الاحتياطية.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project1.jpg"
                                        title="تم إنجاز هذا المشروع بدقة متناهية وإتقان، حيث قمنا  برش الخرسانة للخزانات الاحتياطية."
                                        data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project3.jpeg" class="img-fluid"
                                    alt="تم تنفيذ مشروع تدعيم جدران الحاملة وتدعيم سقف خرساني بنظام شرائح الكاربون فايبر.">
                                <div class="portfolio-info">
                                    <h4>فيلا سكنية</h4>
                                    <p>تم تنفيذ مشروع تدعيم جدران الحاملة وتدعيم سقف خرساني بنظام شرائح الكاربون فايبر.
                                    </p>
                                    <a href="{{ route('home') }}/assets/img/projects/project3.jpeg"
                                        title="تم تنفيذ مشروع تدعيم جدران الحاملة وتدعيم سقف خرساني بنظام شرائح الكاربون فايبر."
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project6.jpeg" class="img-fluid"
                                    alt="من أهم المشاريع المنجزة في مجال حقن التربة.">
                                <div class="portfolio-info">
                                    <h4>استراحة</h4>
                                    <p>من أهم المشاريع المنجزة في مجال حقن التربة.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project6.jpeg"
                                        title="من أهم المشاريع المنجزة في مجال حقن التربة."
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project9.jpg" class="img-fluid"
                                    alt="تم إنجاز أعمال حقن التربة.">
                                <div class="portfolio-info">
                                    <h4>فندق سكني</h4>
                                    <p>تم إنجاز أعمال حقن التربة.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project9.jpg"
                                        title="تم إنجاز أعمال حقن التربة."
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-spraying">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project2.jpg" class="img-fluid"
                                    alt="تم رش خرسانة لتدعيم جدران مسبح.">
                                <div class="portfolio-info">
                                    <h4>فيلا سكنية</h4>
                                    <p>تم رش خرسانة لتدعيم جدران مسبح.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project2.jpg"
                                        title="تم رش خرسانة لتدعيم جدران مسبح."
                                        data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project4.jpeg" class="img-fluid"
                                    alt="تم تنفيذ أعمال تدعيم بلاطة سقف خرساني بنظام شرائح الكاربون فايبر.">
                                <div class="portfolio-info">
                                    <h4>مبنى السيار</h4>
                                    <p>تم تنفيذ أعمال تدعيم بلاطة سقف خرساني بنظام شرائح الكاربون فايبر.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project4.jpeg"
                                        title="تم تنفيذ أعمال تدعيم بلاطة سقف خرساني بنظام شرائح الكاربون فايبر."
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project7.jpeg" class="img-fluid" alt="حقن التربة.">
                                <div class="portfolio-info">
                                    <h4>مجمع فلل</h4>
                                    <p>تم إنجاز مشروع حقن التربة.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project7.jpeg"
                                        title="حقن التربة." data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-stabilization">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project10.jpeg" class="img-fluid"
                                    alt="تم تنفيذ مشروع سند التربة.">
                                <div class="portfolio-info">
                                    <h4>مجمع تجاري</h4>
                                    <p>تم تنفيذ مشروع سند التربة.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project10.jpeg"
                                        title="تم تنفيذ مشروع سند التربة." data-gallery="portfolio-gallery-book"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project12.jpeg" class="img-fluid"
                                    alt="تم تنفيذ مشروع تدعيم بلاطة خرسانية بنظام الجسور الحديدية .">
                                <div class="portfolio-info">
                                    <h4>فيلا سكنية</h4>
                                    <p>تم تنفيذ مشروع تدعيم بلاطة خرسانية بنظام الجسور الحديدية .</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project12.jpeg"
                                        title="تم تنفيذ مشروع تدعيم بلاطة خرسانية بنظام الجسور الحديدية ."
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project5.jpeg" class="img-fluid"
                                    alt="تم تنفيذ أعمال تدعيم حفرية وسند المبنى .">
                                <div class="portfolio-info">
                                    <h4>تدعيم وسند</h4>
                                    <p>تم تنفيذ أعمال تدعيم حفرية وسند المبنى .</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project5.jpeg"
                                        title="تم تنفيذ أعمال تدعيم حفرية وسند المبنى ."
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project8.png" class="img-fluid" alt="حقن التربة.">
                                <div class="portfolio-info">
                                    <h4>مجمع فلل</h4>
                                    <p>تم تنفيذ أعمال حقن التربة.</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project8.png"
                                        title="حقن التربة." data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-stabilization">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project11.jpeg" class="img-fluid"
                                    alt="تم إنجاز أعمال سند التربة">
                                <div class="portfolio-info">
                                    <h4>فيلا سكنية</h4>
                                    <p>تم إنجاز أعمال سند التربة</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project11.jpeg"
                                        title="تم إنجاز أعمال سند التربة" data-gallery="portfolio-gallery-book"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                    </div><!-- End Projects Container -->
                </div>

            </div>
        </section><!-- End Our Projects Section -->

        <!--  Stats Counter Section  -->
        <section class="stats-counter section-bg">
            <div class="container">
                <div class="row gy-4">
                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-emoji-smile color-blue flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="170"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>عميل راضٍ</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-journal-richtext color-orange flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="200"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>مشروع</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-headset color-green flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="2000"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>استشارة مهنية</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-people color-pink flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="15"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>عامل جاد</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                </div>

            </div>
        </section><!-- End Stats Counter Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container" data-aos="fade-up" data-aos-delay="100">
                <div class="row gy-4">
                    <div class="col-lg-6">
                        <div class="info-item  d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-map"></i>
                            <h3>عنواننا</h3>
                            <p><a target="_blank" href="https://goo.gl/maps/USD9LEYxN9CkJFYB7">المملكة العربية
                                    السعودية، الرياض، شارع الشباب
                                </a></p>
                        </div>
                    </div><!-- End Info Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="info-item d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-envelope"></i>
                            <h3>راسلنا</h3>
                            <p><a target="_blank"
                                    href="mailto:abedaltef1989@hotmail.com">abedaltef1989@hotmail.com</a></p>

                        </div>
                    </div><!-- End Info Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="info-item  d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-telephone"></i>
                            <h3>اتصل بنا</h3>
                            <p><a target="_blank" href="tel:0966552319919">0966-55-231-9919</a></p>
                        </div>
                    </div><!-- End Info Item -->

                </div>

                <div class="row gy-4 mt-1">

                    <div class="col-lg-6 ">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3627.989963108149!2d46.7593731!3d24.589542599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f0967f9432ffd%3A0xf34c3c74717a4041!2z2YXYpNiz2LPYqSDYo9i52YXYp9mEINin2YTYqtiv2LnZitmFINmE2YTZhdmC2KfZiNmE2KfYqg!5e0!3m2!1sar!2ssa!4v1693507791527!5m2!1sar!2ssa"
                            frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
                    </div><!-- End Google Maps -->

                    <div class="col-lg-6">
                        <form action="mailto:abedaltef1989@hotmail.com" role="form" class="php-email-form">
                            <div class="row gy-4">
                                <div class="col-lg-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="اسمك" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="إيميلك" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" id="subject"
                                    placeholder="الموضوع" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="body" rows="5" placeholder="نص الرسالة" required></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">جاري الإرسال ..</div>
                                <div class="error-message"></div>
                                <div class="sent-message">تم إرسال الرسالة، شكراً لك.</div>
                            </div>
                            <div class="text-center"><button type="submit">ارسل الرسالة</button></div>
                        </form>
                    </div><!-- End Contact Form -->

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

    <!--  Footer  -->
    <footer id="footer" class="footer">
        <div class="footer-content position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="footer-info">
                            <h3>أعمال التدعيم</h3>
                            <p><a target="_blank" class="link-light" href="https://goo.gl/maps/USD9LEYxN9CkJFYB7">
                                    المملكة العربية السعودية، الرياض، شارع الشباب</a><br><br>
                                <strong>الجوال: </strong> <span dir="rtl">
                                    <a target="_blank" class="link-light"
                                        href="tel:0966552319919">0966-55-231-9919</a></span><br>
                                <strong>الإيميل: </strong>
                                <a target="_blank" class="link-light"
                                    href="mailto:abedaltef1989@hotmail.com">abedaltef1989@hotmail.com</a><br>
                            </p>
                            <div class="social-links d-flex mt-3">
                                <a href="https://twitter.com/_4536618901303"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-twitter"></i></a>
                                <a href="https://wa.me/+966554782271"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-whatsapp"></i></a>
                                <a href="https://www.tiktok.com/@7mood1418?_t=8f3UaDUSWsD&_r=1"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-tiktok"></i></a>
                                <a href="https://t.snapchat.com/P1Nr0m4U"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-snapchat"></i></a>
                            </div>
                        </div>
                    </div><!-- End footer info column-->

                    <div class="col-lg-4 col-md-6 footer-links">
                        <h4>روابط سريعة</h4>
                        <ul>
                            <li><a href="{{ route('ar') }}">الرئيسية</a></li>
                            <li><a href="{{ route('ar') }}#about">من نحن</a></li>
                            <li><a href="{{ route('ar') }}#services">خدماتنا</a></li>
                            <li><a href="{{ route('ar') }}#projects">مشاريعنا</a></li>
                            <li><a href="{{ route('ar') }}#contact">تواصل معنا</a></li>
                        </ul>
                    </div><!-- End footer links column-->

                    <div class="col-lg-4 col-md-6 footer-links">
                        <h4>خدماتنا</h4>
                        <ul>
                            <li><a href="{{ route('ar') }}#service1">إصلاح الخرسانة</a></li>
                            <li><a href="{{ route('ar') }}#service2">التدعيم بنظام القمصان الخرسانية</a></li>
                            <li><a href="{{ route('ar') }}#service3">أعمال الكاربون فايبر</a></li>
                            <li><a href="{{ route('ar') }}#service4">التدعيم بنظام الأوتاد الخرسانية</a></li>
                            <li><a href="{{ route('ar') }}#service5">حقن التربة</a></li>
                            <li><a href="{{ route('ar') }}#service6">سند التربة</a></li>
                        </ul>
                    </div><!-- End footer links column-->

                </div>
            </div>
        </div>

        <div class="footer-legal text-center position-relative">
            <div class="container">
                <div class="copyright">
                    &copy; حقوق نشر
                    <strong><span><a href="{{ route('ar') }}">أعمال التدعيم</a></span></strong>.
                    كل الحقوق محفوظة
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <!-- Script to add active class to nav item when click on it -->
    <script>
        const navItems = document.querySelectorAll('.nav-link');

        navItems.forEach(item => {
            item.addEventListener('click', () => {
                // Remove active class from all items
                navItems.forEach(item => {
                    item.classList.remove('active');
                });
                // Add active class to clicked item
                item.classList.add('active');
            });
        });
    </script>
</body>

</html>
