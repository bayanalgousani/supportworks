<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->isLocale('ar') ? 'rtl' : 'ltr' }}">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ trans('views.title') }}</title>
    <meta
        content="Support Works Contracting Co. for contracting and Construction in the Kingdom of Saudi Arabia is the best company for Concrete repair , Building Reinforcement ,Building evaluation, Reinforcement using concrete shirts , Carbon fiber works , Reinforcing with concrete wedges , Soil injection , Soil injection shotcerte and Soil stabilization, browse SupportsWorks website and contact us"
        name="description">
    <meta
        content="support works,Contracting, contracting, concrete repair, building reinforcement , building evaluation, column support, beam support , slab support , foundation support , reinforcement using concrete shirts , carbon fiber works , reinforcing with concrete wedges , soil injection , carbon fiber , shotcrete , soil injection shotcerte , soil stabilization,  support works contractor, support works company, support works contractor company, support works contractor company in saudi arabia ,support works in saudi arabia"
        name="keywords">
    <meta content="Support Works Team" name="author">
    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link rel="alternate" hreflang="en" href="https://supports-works.com" />
    <link rel="canonical" href="https://supports-works.com" />

    <!-- Google Fonts -->
    <link rel="preconnect" href="{{ asset('https://fonts.googleapis.com') }}">
    <link rel="preconnect" href="{{ asset('https://fonts.gstatic.com') }}" crossorigin>
    <link
        href="{{ asset('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap') }}"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7ZVK54NV7E"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7ZVK54NV7E');
    </script>

</head>

<body>

    <!--  Header  -->
    <header id="header" class="header d-flex align-items-center">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="{{ route('home') }}" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <img src="{{ asset('assets/img/favicon.png') }}" alt="{{ trans('views.title') }}">
                <h1>{{ trans('views.title') }}<span>.</span></h1>
            </a>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="{{ route('home') }}" class="nav-link active">{{ trans('views.home') }}</a></li>
                    <li><a href="{{ route('home') }}#about" class="nav-link">{{ trans('views.about') }}</a></li>
                    <li><a href="{{ route('home') }}#services" class="nav-link">{{ trans('views.services') }}</a></li>
                    <li><a href="{{ route('home') }}#projects" class="nav-link">{{ trans('views.projects') }}</a></li>
                    <li><a href="{{ route('home') }}#contact" class="nav-link">{{ trans('views.contact') }}</a></li>
                    <li><a href="@if (app()->isLocale('en')) {{ route('ar') }} @else {{ route('switched-en') }} @endif"
                            class="nav-link">
                            @if (app()->isLocale('en'))
                                عربي
                            @else
                                English
                            @endif
                        </a></li>

                    <li>
                        <a href="tel:0966552319919" class="btn-primary">
                            call us
                        </a>
                    </li>
                </ul>
            </nav><!-- .navbar -->

        </div>
    </header>
    <!-- End Header -->

    <!--  Hero Section  -->
    <section id="hero" class="hero">
        <div class="info d-flex align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h2 data-aos="fade-down">{{ trans('views.welcome') }}</h2>
                        <p data-aos="fade-up"> {{ trans('views.description') }}</p>
                        <a data-aos="fade-up" data-aos-delay="200"
                            href="{{ route('home') }}/files/support_works_profile.pdf" Download="support_works_profile"
                            class="btn-get-started">{{ trans('views.downloadProfile') }}</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000" dir="ltr">
            <div class="carousel-item active"
                style="background-image: url(assets/img/hero-carousel/hero-carousel-1.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-2.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-3.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-4.jpg)">
            </div>
            <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/hero-carousel-5.jpg)">
            </div>

            <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
            </a>

            <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </section><!-- End Hero Section -->

    <main id="main">

        <!--  About us Section  -->
        <section id="about" class="alt-services">
            <div class="container" data-aos="fade-up">

                <div class="row justify-content-around gy-4">
                    <div role="img" class="col-lg-6 img-bg"
                        style="background-image: url(assets/img/alt-services.jpg);" data-aos="zoom-in"
                        data-aos-delay="100" aria-label="{{ trans('views.description') }}"></div>

                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                        <h3>{{ trans('views.about') }}</h3>
                        <p>{{ trans('views.about.description') }}
                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="100">
                            <i class="bi bi-patch-check flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">{{ trans('views.about.check1') }}</span>
                                </h4>
                                <p>{{ trans('views.about.check1.desc') }}</p>
                            </div>
                        </div><!-- End Icon Box -->

                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="200">
                            <i class="bi bi-patch-check flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">{{ trans('views.about.check2') }}</span>
                                </h4>
                                <p>{{ trans('views.about.check2.desc') }}</p>
                            </div>
                        </div><!-- End Icon Box -->

                        <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="300">
                            <i class="bi bi-brightness-high flex-shrink-0"></i>
                            <div>
                                <h4><span class="stretched-link">{{ trans('views.about.check3') }}</span>
                                </h4>
                                <p>{{ trans('views.about.check3.desc') }}</p>
                            </div>
                        </div><!-- End Icon Box -->

                    </div>
                </div>

            </div>
        </section><!-- End Alt Services Section -->

        <!--  Services Section  -->
        <section id="services" class="services section-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>{{ trans('views.ourServices') }}</h2>
                    <p>{{ trans('views.services.description') }}</p>
                </div>

                <div class="row gy-4">

                    <div id="service1" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="service-item  position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-mountain-city"></i>
                            </div>
                            <h3>{{ trans('views.service1') }}</h3>
                            <p>{{ trans('views.service1.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service2" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-arrow-up-from-ground-water"></i>
                            </div>
                            <h3>{{ trans('views.service2') }}</h3>
                            <p>{{ trans('views.service2.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service3" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-compass-drafting"></i>
                            </div>
                            <h3>{{ trans('views.service3') }}</h3>
                            <p>{{ trans('views.service3.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service4" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-trowel-bricks"></i>
                            </div>
                            <h3>{{ trans('views.service4') }}</h3>
                            <p>{{ trans('views.service4.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service5" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-helmet-safety"></i>
                            </div>
                            <h3>{{ trans('views.service5') }}</h3>
                            <p>{{ trans('views.service5.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div id="service6" class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="600">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="fa-solid fa-arrow-up-from-ground-water"></i>
                            </div>
                            <h3>{{ trans('views.service6') }}</h3>
                            <p>{{ trans('views.service6.description') }}</p>
                        </div>
                    </div><!-- End Service Item -->

                </div>

            </div>
        </section><!-- End Services Section -->

        <!--  Our Projects Section  -->
        <section id="projects" class="projects">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>{{ trans('views.projects') }}</h2>
                    <p>{{ trans('views.projects.description') }}</p>
                </div>

                <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry"
                    data-portfolio-sort="original-order">

                    <ul class="portfolio-flters" data-aos="fade-up" data-aos-delay="100">
                        <li data-filter="*" class="filter-active">{{ trans('views.project.category1') }}</li>
                        <li data-filter=".filter-concrete-spraying">{{ trans('views.project.category2') }}</li>
                        <li data-filter=".filter-concrete-reinforcement">{{ trans('views.project.category3') }}</li>
                        <li data-filter=".filter-soil-injection">{{ trans('views.project.category4') }}</li>
                        <li data-filter=".filter-soil-stabilization">{{ trans('views.project.category5') }}</li>
                    </ul><!-- End Projects Filters -->

                    <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-spraying">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project1.jpg" class="img-fluid"
                                    alt="{{ trans('views.project1.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project1') }}</h4>
                                    <p>{{ trans('views.project1.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project1.jpg"
                                        title="{{ trans('views.project1.description') }}"
                                        data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project3.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project3.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project3') }}</h4>
                                    <p>{{ trans('views.project3.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project3.jpeg"
                                        title="{{ trans('views.project3.description') }}"
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project6.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project6.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project6') }}</h4>
                                    <p>{{ trans('views.project6.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project6.jpeg"
                                        title="{{ trans('views.project6.description') }}"
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project9.jpg" class="img-fluid"
                                    alt="{{ trans('views.project9.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project9') }}</h4>
                                    <p>{{ trans('views.project9.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project9.jpg"
                                        title="{{ trans('views.project9.description') }}"
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-spraying">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project2.jpg" class="img-fluid"
                                    alt="{{ trans('views.project2.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project2') }}</h4>
                                    <p>{{ trans('views.project2.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project2.jpg"
                                        title="{{ trans('views.project2.description') }}"
                                        data-gallery="portfolio-gallery-remodeling" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project4.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project4.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project4') }}</h4>
                                    <p>{{ trans('views.project4.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project4.jpeg"
                                        title="{{ trans('views.project4.description') }}"
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project7.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project7.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project7') }}</h4>
                                    <p>{{ trans('views.project7.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project7.jpeg"
                                        title="{{ trans('views.project7.description') }}"
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-stabilization">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project10.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project10.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project10') }}</h4>
                                    <p>{{ trans('views.project10.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project10.jpeg"
                                        title="{{ trans('views.project10.description') }}"
                                        data-gallery="portfolio-gallery-book" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project12.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project12.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project12') }}</h4>
                                    <p>{{ trans('views.project12.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project12.jpeg"
                                        title="{{ trans('views.project12.description') }}"
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-concrete-reinforcement">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project5.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project5.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project5') }}</h4>
                                    <p>{{ trans('views.project5.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project5.jpeg"
                                        title="{{ trans('views.project5.description') }}"
                                        data-gallery="portfolio-gallery-concrete-reinforcement"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-injection">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project8.png" class="img-fluid"
                                    alt="{{ trans('views.project8.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project8') }}</h4>
                                    <p>{{ trans('views.project8.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project8.png"
                                        title="{{ trans('views.project8.description') }}"
                                        data-gallery="portfolio-gallery-soil-injection"
                                        class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                        <div class="col-lg-4 col-md-6 portfolio-item filter-soil-stabilization">
                            <div class="portfolio-content h-100">
                                <img src="assets/img/projects/project11.jpeg" class="img-fluid"
                                    alt="{{ trans('views.project11.description') }}">
                                <div class="portfolio-info">
                                    <h4>{{ trans('views.project11') }}</h4>
                                    <p>{{ trans('views.project11.description') }}</p>
                                    <a href="{{ route('home') }}/assets/img/projects/project11.jpeg"
                                        title="{{ trans('views.project11.description') }}"
                                        data-gallery="portfolio-gallery-book" class="glightbox preview-link"><i
                                            class="bi bi-zoom-in"></i></a>
                                </div>
                            </div>
                        </div><!-- End Projects Item -->

                    </div><!-- End Projects Container -->
                </div>

            </div>
        </section><!-- End Our Projects Section -->

        <!--  Stats Counter Section  -->
        <section class="stats-counter section-bg">
            <div class="container">
                <div class="row gy-4">
                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-emoji-smile color-blue flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="170"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>{{ trans('views.statistics1') }}</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-journal-richtext color-orange flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="200"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>{{ trans('views.statistics2') }}</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-headset color-green flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="2000"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>{{ trans('views.statistics3') }}</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="stats-item d-flex align-items-center w-100 h-100">
                            <i class="bi bi-people color-pink flex-shrink-0"></i>
                            <div>
                                <span data-purecounter-start="0" data-purecounter-end="15"
                                    data-purecounter-duration="1" class="purecounter"></span>
                                <p>{{ trans('views.statistics4') }}</p>
                            </div>
                        </div>
                    </div><!-- End Stats Item -->

                </div>

            </div>
        </section><!-- End Stats Counter Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container" data-aos="fade-up" data-aos-delay="100">
                <div class="row gy-4">
                    <div class="col-lg-6">
                        <div class="info-item  d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-map"></i>
                            <h3>{{ trans('views.contact.address.title') }}</h3>
                            <p><a target="_blank"
                                    href="https://goo.gl/maps/USD9LEYxN9CkJFYB7">{{ trans('views.contact.address') }}
                                </a></p>
                        </div>
                    </div><!-- End Info Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="info-item d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-envelope"></i>
                            <h3>{{ trans('views.contact.email.title') }}</h3>
                            <p><a target="_blank"
                                    href="mailto:abedaltef1989@hotmail.com">abedaltef1989@hotmail.com</a></p>

                        </div>
                    </div><!-- End Info Item -->

                    <div class="col-lg-3 col-md-6">
                        <div class="info-item  d-flex flex-column justify-content-center align-items-center">
                            <i class="bi bi-telephone"></i>
                            <h3>{{ trans('views.contact.call.title') }}</h3>
                            <p><a target="_blank" href="tel:0966552319919">0966-55-231-9919</a></p>
                        </div>
                    </div><!-- End Info Item -->

                </div>

                <div class="row gy-4 mt-1">

                    <div class="col-lg-6 ">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3627.989963108149!2d46.7593731!3d24.589542599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f0967f9432ffd%3A0xf34c3c74717a4041!2z2YXYpNiz2LPYqSDYo9i52YXYp9mEINin2YTYqtiv2LnZitmFINmE2YTZhdmC2KfZiNmE2KfYqg!5e0!3m2!1sar!2ssa!4v1693507791527!5m2!1sar!2ssa"
                            frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
                    </div><!-- End Google Maps -->

                    <div class="col-lg-6">
                        <form action="mailto:abedaltef1989@hotmail.com" role="form" class="php-email-form">
                            <div class="row gy-4">
                                <div class="col-lg-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="{{ trans('views.contact.name') }}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="{{ trans('views.contact.email') }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" id="subject"
                                    placeholder="{{ trans('views.contact.subject') }}" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="body" rows="5" placeholder="{{ trans('views.contact.message') }}"
                                    required></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">{{ trans('views.contact.loading') }}</div>
                                <div class="error-message"></div>
                                <div class="sent-message">{{ trans('views.contact.sentMessageSuccess') }}</div>
                            </div>
                            <div class="text-center"><button
                                    type="submit">{{ trans('views.contact.send') }}</button></div>
                        </form>
                    </div><!-- End Contact Form -->

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

    <!--  Footer  -->
    <footer id="footer" class="footer">
        <div class="footer-content position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="footer-info">
                            <h3>{{ trans('views.title') }}</h3>
                            <p><a target="_blank" class="link-light" href="https://goo.gl/maps/USD9LEYxN9CkJFYB7">
                                    {{ trans('views.contact.address') }}</a><br><br>
                                <strong>{{ trans('views.footer.phone') }}</strong> <span dir="rtl">
                                    <a target="_blank" class="link-light"
                                        href="tel:0966552319919">0966-55-231-9919</a></span><br>
                                <strong>{{ trans('views.footer.email') }}</strong>
                                <a target="_blank" class="link-light"
                                    href="mailto:abedaltef1989@hotmail.com">abedaltef1989@hotmail.com</a><br>
                            </p>
                            <div class="social-links d-flex mt-3">
                                <a href="https://twitter.com/_4536618901303"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-twitter"></i></a>
                                <a href="https://wa.me/+966554782271"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-whatsapp"></i></a>
                                <a href="https://www.tiktok.com/@7mood1418?_t=8f3UaDUSWsD&_r=1"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-tiktok"></i></a>
                                <a href="https://t.snapchat.com/P1Nr0m4U"
                                    class="d-flex align-items-center justify-content-center" target="_blank"><i
                                        class="bi bi-snapchat"></i></a>
                            </div>
                        </div>
                    </div><!-- End footer info column-->

                    <div class="col-lg-4 col-md-6 footer-links">
                        <h4>{{ trans('views.footer.usefulLinks') }}</h4>
                        <ul>
                            <li><a href="{{ route('home') }}">{{ trans('views.home') }}</a></li>
                            <li><a href="{{ route('home') }}#about">{{ trans('views.about') }}</a></li>
                            <li><a href="{{ route('home') }}#services">{{ trans('views.ourServices') }}</a></li>
                            <li><a href="{{ route('home') }}#projects">{{ trans('views.projects') }}</a></li>
                            <li><a href="{{ route('home') }}#contact">{{ trans('views.contact') }}</a></li>
                        </ul>
                    </div><!-- End footer links column-->

                    <div class="col-lg-4 col-md-6 footer-links">
                        <h4>{{ trans('views.ourServices') }}</h4>
                        <ul>
                            <li><a href="{{ route('home') }}#service1">{{ trans('views.service1') }}</a></li>
                            <li><a href="{{ route('home') }}#service2">{{ trans('views.service2') }}</a></li>
                            <li><a href="{{ route('home') }}#service3">{{ trans('views.service3') }}</a></li>
                            <li><a href="{{ route('home') }}#service4">{{ trans('views.service4') }}</a></li>
                            <li><a href="{{ route('home') }}#service5">{{ trans('views.service5') }}</a></li>
                            <li><a href="{{ route('home') }}#service6">{{ trans('views.service6') }}</a></li>
                        </ul>
                    </div><!-- End footer links column-->

                </div>
            </div>
        </div>

        <div class="footer-legal text-center position-relative">
            <div class="container">
                <div class="copyright">
                    &copy; {{ trans('views.footer.copyright') }}
                    <strong><span><a href="{{ route('home') }}">{{ trans('views.title') }}</a></span></strong>.
                    {{ trans('views.footer.allRightsReserved') }}
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <!-- Script to add active class to nav item when click on it -->
    <script>
        const navItems = document.querySelectorAll('.nav-link');

        navItems.forEach(item => {
            item.addEventListener('click', () => {
                // Remove active class from all items
                navItems.forEach(item => {
                    item.classList.remove('active');
                });

                // Add active class to clicked item
                item.classList.add('active');
            });
        });
    </script>
</body>

</html>
