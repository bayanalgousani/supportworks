<?php

return [

    'home' => 'Home',
    'seo.desc' => 'Support Works Contracting Co. for contracting and Construction in the Kingdom of Saudi Arabia is the best company for Concrete repair , Building Reinforcement ,Building evaluation, Reinforcement using concrete shirts , Carbon fiber works , Reinforcing with concrete wedges , Soil injection , Soil injection shotcerte and Soil stabilization, browse SupportsWorks website and contact us',
    'seo.keywords' => 'support works,Contracting, contracting, concrete repair, building reinforcement , building evaluation, reinforcement using concrete shirts , carbon fiber works , reinforcing with concrete wedges , soil injection , soil injection shotcerte , soil stabilization,  support works contractor, support works company, support works contractor company, support works contractor company in saudi arabia ,support works in saudi arabia',

    'seo.author' => 'Support Works Team',
    'about' => 'About',
    'ourServices' => 'Our Services',
    'projects' => 'Projects',
    'contact' => 'Contact',
    'title' => 'Support Works',
    'welcome' => 'Welcome to Support Works',
    'description' => 'Support Works Contracting Corporation in the Kingdom of Saudi Arabia for urban development in all its fields.',
    'downloadProfile' => 'Download Profile',
    'about' => 'About Us',

    'about.description' => 'Support Works company in the Kingdom of Saudi Arabia, which includes a group of young professionals and technicians specialized in building and developing facilities in all its commercial, industrial and residential fields. We offer many services, and we are distinguished by several features that make you choose us, including:',
    'about.check1' => 'Quality of work and equipment',
    'about.check1.desc' => 'We have modern equipment that provides you with quality performance, and we also follow the latest urban developments globally, and the latest methods used to ensure high quality, and we put our technical expertise and modern repair equipment in your hands, hoping to make it a positive tool.',
    'about.check2' => 'Experience and professionalism',
    'about.check2.desc' => 'Support Works has a team of experts in determining the seriousness of facility problems accurately and professionally, finding a quick and technical solution and following up on the repair process.',
    'about.check3' => 'Creative solutions',
    'about.check3.desc' => 'Support Works provides creative, effective and fast solutions to satisfy its customers and maintain the rules and principles of its work. These solutions include buildings, soil remediation and all the services they provide.',

    'services' => 'Services',
    'services.description' => 'Support Works Company provides many services related to architectural structures and soil treatment in the highest quality and in the shortest possible time.',

    'service1' => 'Concrete repair',
    'service1.description' => 'We provide concrete repair services for all types of damage, from cracks to severe damage. Our tailored approach ensures effective and customized solutions to restore your concrete to its original condition.',

    'service2' => 'Reinforcement using concrete shirts',
    'service2.description' => 'Concrete reinforcement is done by increasing the concrete sections of the columns and supporting bridges through the addition of a mesh of delivery iron and embedding it in the old concrete according to the required specifications. The concrete is then poured according to appropriate conditions.',

    'service3' => 'Carbon fiber works',
    'service3.description' => 'The structural element is prepared to withstand greater forces than its previous capacity, or in case of weakness due to certain factors, by treating it instead of enlarging its concrete section or adding iron sections to strengthen it, using materials from international companies to reinforce the structural elements whether they are reinforced concrete, prestressed concrete or iron elements.',

    'service4' => 'Reinforcing with concrete wedges',
    'service4.description' => 'We provide a solution for buildings experiencing foundation and flooring settlement by constructing reinforced concrete piles to transfer the weight of the building from the foundation to appropriate layers of soil. These piles are driven through the foundation until they reach a suitable foundation layer, and the foundation then acts as a cap for the piles.',

    'service5' => 'Soil injection',
    'service5.description' => 'One of the most important soil treatment works is injecting it using cement mortar and chemicals to raise the concrete stress, thus obtaining an injected area free of voids and cavities.',

    'service6' => 'Soil stabilization',
    'service6.description' => 'In case of difficulty in pouring ready-mixed concrete, we offer soil stabilization services where we spray cement for various works using the latest equipment, thus providing cost and time savings compared to wooden formwork.',


    'projects' => 'Our Projects',
    'projects.description' => 'More than two hundred projects have been completed in various fields and in several places in the Kingdom. We give you an overview of what we have presented.',
    'project.category1' => 'All',
    'project.category2' => 'Concrete spraying',
    'project.category3' => 'Concrete reinforcement',
    'project.category4' => 'Soil injection',
    'project.category5' => 'Soil stabilization',

    'project1' => 'Concrete spraying for company', // category 1
    'project1.description' => 'This project was completed successfully, during which concrete was sprayed for the reserve tanks.',
    'project2' => 'Residential villa', // category 2
    'project2.description' => 'This project was completed successfully, during which concrete was sprayed to reinforce the walls of a swimming pool.',


    'project3' => 'Residential villa', // category 3
    'project3.description' => 'The project of reinforcing load-bearing walls and reinforcing a concrete roof with a carbon fiber slats system has been implemented.',
    'project4' => 'Al-sayar building', // category 3
    'project4.description' => 'Reinforcement of a concrete roof slab with a carbon fiber slats system was carried out',
    'project5' => 'Ministry of Health', // category 3
    'project5.description' => 'Excavation and sanitary building support works have been carried out',
    'project12' => 'Residential villa', // category 3
    'project12.description' => 'The project of reinforcing a concrete slab with a system of iron bridges was implemented',

    'project6' => 'Break', // category 4
    'project6.description' => 'One of the most important completed projects in the field of soil injection.',
    'project7' => 'Villa compound in Al Maather', // category 4
    'project7.description' => 'The soil injection project was completed.',
    'project8' => 'villa compound', // category 4
    'project8.description' => 'Soil injection works have been carried out.',
    'project9' => 'Residential hotel', // category 4
    'project9.description' => 'Soil injection works have been completed.',

    'project10' => 'Al-Aqiq Commercial Compound', // category 5
    'project10.description' => 'The soil stabilization project was implemented.',
    'project11' => 'Residential villa', // category 5
    'project11.description' => 'The soil stabilization works have been completed.',

    'statistics1' => 'Happy Clients',
    'statistics2' => 'Projects',
    'statistics3' => 'Consultations',
    'statistics4' => 'Hard Workers',


    'contact' => 'Contact Us',
    'contact.description' => 'If you have any questions, please contact us.',
    'contact.name' => 'Your Name',
    'contact.email' => 'Your Email',
    'contact.message' => 'Message',
    'contact.send' => 'Send Message',
    'contact.subject' => 'Subject',
    'contact.address.title' => 'Our Address',
    'contact.address' => 'Kingdom of Saudi Arabia, Riyadh, Al Shabab Street',
    'contact.email.title' => 'Email Us',
    'contact.call.title' => 'Call Us',
    'contact.sentMessageSuccess' => 'Your message has been sent. Thank you!',
    'contact.sentMessageError' => 'Your message hasn\'t been sent. Please try again.',
    'contact.loading' => 'Loading..',




    'footer.email' => 'Email: ',
    'footer.phone' => 'Phone: ',
    'footer.usefulLinks' => 'Useful Links',
    'footer.copyright' => 'Copyright',
    'footer.allRightsReserved' => 'All Rights Reserved',
];
