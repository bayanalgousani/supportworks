<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home')->middleware('set.locale');

Route::get('ar', function () {
    return view('index_ar');
})->name('ar')->middleware('set.locale');

###################### Translation ######################
Route::get('switched-en', function () {
    session(['locale' => 'en']);
    return redirect()->to(route('home'));
})->name('switched-en');
